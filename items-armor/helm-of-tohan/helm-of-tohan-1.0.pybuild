# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
from pybuild import Pybuild1, InstallDir, File, apply_patch


class Package(Pybuild1):
    NAME = "Helm of Tohan"
    DESC = "Includes a new quest to find the titular, legendary artifact"
    HOMEPAGE = """
        https://elderscrolls.bethesda.net/en/morrowind
        https://gitlab.com/bmwinger/umopp
    """
    # Original is all-rights-reserved
    # UMOPP is attribution
    LICENSE = "all-rights-reserved attribution"
    RESTRICT = "mirror"
    RDEPEND = "base/morrowind"
    KEYWORDS = "openmw"
    SRC_URI = """
        https://cdn.bethsoft.com/elderscrolls/morrowind/other/ebartifact.zip
        https://gitlab.com/bmwinger/umopp/uploads/ec492fea9914df33232814ff9282d972/helmoftohan-umopp-3.0.2.tar.xz
    """
    INSTALL_DIRS = [InstallDir(".", PLUGINS=[File("EBQ_Artifact.esp")], S="ebartifact")]

    def src_prepare(self):
        # From instructions in README.md
        path = os.path.join(self.WORKDIR, "helmoftohan-umopp-3.0.2")
        apply_patch(os.path.join(path, "EBQ_Artifact_plugin.patch"))
        apply_patch(os.path.join(path, "EB_Helm_of_Tohan_mesh.patch"))
        os.remove("Icons/A/TX_EB_helm_of_Tohan.tga")
        os.remove("Textures/TX_EB_Helm of Tohan.BMP")
