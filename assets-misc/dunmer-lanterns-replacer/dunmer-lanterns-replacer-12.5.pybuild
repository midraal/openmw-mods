# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import InstallDir, Pybuild1

from common.nexus import NexusMod


class Package(NexusMod, Pybuild1):
    NAME = "Dunmer Lanterns Replacer"
    DESC = "Replaces the Dunmer lanterns from morrowind with smooth, detailed versions"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/43219"
    # Note: We handle this in an unintuitive manner, however the README states that
    # Derivatives are allowed under the conditions of attribution and non-commercial use
    LICENSE = "attribution-nc free-derivation"
    KEYWORDS = "openmw"
    SRC_URI = "Dunmer_Lanterns_Replacer-43219-12-5-1649213618.7z"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/43219"
    TIER = 1
    IUSE = "textures-pherim textures-swg minimal tr glow"
    RDEPEND = """
        tr? ( landmasses/tamriel-rebuilt )
    """
    REQUIRED_USE = "?? ( textures-pherim textures-swg )"
    INSTALL_DIRS = [
        InstallDir("00 Core"),
        InstallDir("01 Glow Effect", REQUIRED_USE="glow"),
        InstallDir(
            "02 Ashlander Lantern (Smoothed Only)", REQUIRED_USE="minimal !glow"
        ),
        InstallDir(
            "02 Ashlander Lantern (Smoothed Only) - Glow Effect",
            REQUIRED_USE="minimal glow",
        ),
        InstallDir(
            "03 Ashlander Lanterns Retexture 1",
            REQUIRED_USE="textures-pherim",
        ),
        InstallDir(
            "03 Ashlander Lanterns Retexture 2",
            REQUIRED_USE="textures-swg",
        ),
        InstallDir("04 Tamriel_Data Patch", REQUIRED_USE="tr !glow"),
        InstallDir("04 Tamriel_Data Patch - Glow Effect", REQUIRED_USE="tr glow"),
    ]
