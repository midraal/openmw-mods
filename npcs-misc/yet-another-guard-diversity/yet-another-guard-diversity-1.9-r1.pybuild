# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild import File, InstallDir, Pybuild1
from common.nexus import NexusMod


class Package(NexusMod, Pybuild1):
    NAME = "Yet Another Guard Diversity"
    DESC = "Replaces the generic guards of Morrowind with different variations"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/45894"
    KEYWORDS = "openmw"
    LICENSE = "all-rights-reserved"
    NEXUS_URL = """
        https://www.nexusmods.com/morrowind/mods/45894
        https://www.nexusmods.com/morrowind/mods/47583
    """
    RDEPEND = "base/morrowind[bloodmoon,tribunal]"
    TEXTURE_SIZES = "256"
    SRC_URI = """
        !minimal? (
            !full-cephalopod? (
                Yet_Another_Guard_Diversity_-_Regular-45894-1-9-1608400137.zip
            )
        )
        full-cephalopod? (
            Yet_Another_Guard_Diversity_-_Full_Cephalopod-45894-1-9-1608400210.zip
        )
        minimal? ( Yet_Another_Guard_Diversity_-_Purist-45894-1-9-1608400296.zip )
        tr? ( https://gitlab.com/portmod/mirror/-/blob/master/Yet_Another_Guard_Diversity_Tamriel_Rebuilt_Imperials-47583-21-01-1612127973.zip -> Yet_Another_Guard_Diversity_Tamriel_Rebuilt_Imperials-47583-21-01-1612127973.zip )
    """
    IUSE = "minimal +full-cephalopod tr"
    REQUIRED_USE = "?? ( minimal full-cephalopod )"
    INSTALL_DIRS = [
        InstallDir(
            ".",
            S="Yet_Another_Guard_Diversity_-_Regular-45894-1-9-1608400137",
            PLUGINS=[File("Yet Another Guard Diversity - Regular.ESP")],
            REQUIRED_USE="!minimal !full-cephalopod",
        ),
        InstallDir(
            ".",
            S="Yet_Another_Guard_Diversity_-_Full_Cephalopod-45894-1-9-1608400210",
            PLUGINS=[File("Yet Another Guard Diversity - Full Cephalopod.ESP")],
            REQUIRED_USE="full-cephalopod",
        ),
        InstallDir(
            ".",
            S="Yet_Another_Guard_Diversity_-_Purist-45894-1-9-1608400296",
            PLUGINS=[File("Yet Another Guard Diversity - Purist.ESP")],
            REQUIRED_USE="minimal",
        ),
        InstallDir(
            ".",
            S="Yet_Another_Guard_Diversity_Tamriel_Rebuilt_Imperials-47583-21-01-1612127973",
            PLUGINS=[File("Yet Another Guard Diversity - TR.ESP")],
            REQUIRED_USE="tr",
        ),
    ]

    # Modular option:
    # InstallDir(
    #    ".",
    #    S="Yet_Another_Guard_Diversity_-_Fully_Modular-45894-1-8-1588106527",
    #    PLUGINS=[
    #        File("Yet Another Guard Diversity - Imperial.ESP"),
    #        File("Yet Another Guard Diversity - Telvanni (Regular).ESP"),
    #        File("Yet Another Guard Diversity - Ordinator.ESP"),
    #        File("Yet Another Guard Diversity - Hlaalu.ESP"),
    #        File("Yet Another Guard Diversity - Telvanni (Full Cephalopod).ESP"),
    #        File("Yet Another Guard Diversity - Redoran.ESP"),
    #        File("Yet Another Guard Diversity - Telvanni (Purist).ESP"),
    #    ],
    # ),
    # Yet_Another_Guard_Diversity_-_Fully_Modular-45894-1-8-1588106527.7z
    # For ANtares Big Mod
    # YAGD_Antares_Big_Mod_Compatibility_Patch-45894-1-0.7z
    # InstallDir(
    #     ".",
    #     S="YAGD_Antares_Big_Mod_Compatibility_Patch-45894-1-0",
    #     PLUGINS=[File("YAGD Antares Big Mod Compatibility Patch.esp")],
    # ),
    # For LGNPC PAX Redoran
    # Yet_Another_Guard_Diversity_-_LGNPC_PAX_Redoran_patch-45894-1-0-1589712377.7z
    # InstallDir(
    #    ".",
    #    S="Yet_Another_Guard_Diversity_-_LGNPC_PAX_Redoran_patch-45894-1-0-1589712377",
    #    PLUGINS=[
    #        File("Yet Another Guard Diversity - LGNPC PAX Redoran patch .ESP")
    #    ],
    # ),
