# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import Pybuild1


class Package(Pybuild1):
    NAME = "Graphics Overhaul Metamod"
    DESC = 'This is the graphics-only version of the "Total Overhaul" list.'
    HOMEPAGE = "https://modding-openmw.com/lists/graphics-overhaul/"
    LICENSE = "CC-BY-SA-4.0"
    KEYWORDS = "openmw"
    IUSE = "+minimal"
    RDEPEND = """
        base/morrowind
        assets-meshes/mesh-fix
        assets-meshes/correct-meshes
        assets-meshes/correct-uv-rocks
        base/patch-for-purists
        landmasses/tamriel-rebuilt
        landmasses/province-cyrodiil
        landmasses/skyrim-home-of-the-nords
        base/tomb-of-the-snow-prince[minimal?,grass]
        assets-meshes/properly-smoothed-meshes
        assets-meshes/ingredients-mesh-replacer
        assets-misc/morrowind-optimization-patch
        assets-meshes/dwemer-mesh-improvement
        assets-meshes/rr-better-meshes
        arch-misc/glow-in-the-dahrk[dark-molag-mar]
        gameplay-misc/graphic-herbalism
        assets-misc/project-atlas
        assets-textures/intelligent-textures
        assets-textures/tyddys-landscape-retexture
        assets-textures/tyddys-daedric-ruins
        assets-textures/tyddys-redoran
        assets-meshes/telvanni-mesh-improvement
        assets-textures/tyddys-telvanni
        arch-towns/tyddys-necrom
        arch-towns/dark-molag-mar
        arch-towns/red-vos
        arch-towns/white-suran-md
        assets-misc/dunmer-lanterns-replacer
        assets-misc/aof-containers
        assets-textures/detailed-tapestries
        assets-textures/ast-beds-remastered
        assets-misc/better-kegstands
        assets-textures/arukinns-better-books-and-scrolls
        assets-textures/guar-skin-banners
        assets-misc/improved-kwama-eggs-and-egg-sacs[graphic-herbalism]
        assets-textures/one-true-faith
        assets-misc/tyddys-dunmeri-urns
        assets-textures/long-live-the-limeware
        assets-textures/long-live-the-glassware
        assets-textures/long-live-the-plates
        items-misc/improved-better-skulls
        assets-misc/apels-various-things-signs
        assets-misc/pherim-bread
        assets-misc/pherim-cavern-clutter
        assets-misc/pherim-guar-cart
        assets-misc/bloodmoon-hide-replacer
        items-misc/dagoth-ur-welcomes-you
        items-misc/illuminated-palace-of-vivec
        gameplay-misc/containers-animated
        items-misc/redoran-council-hall-improvement
        assets-misc/soulgem-replacer
        assets-textures/tyddys-shacks-docks-and-ships
        assets-textures/tyddys-sewers
        arch-towns/mournhold-overhaul
        arch-towns/distant-mournhold
        assets-textures/facelift
        npcs-bodies/new-beast-bodies
        npcs-bodies/animation-compilation
        npcs-misc/yet-another-guard-diversity
        npcs-voices/dagoth-ur-voice-addon
        npcs-voices/almalexia-voice
        npcs-bodies/dirnaes-beast-animations
        npcs-bodies/almalexias-cast-for-beasts
        npcs-misc/starfires-npcs
        npcs-voices/vivec-voice-addon
        items-clothes/better-clothes
        arch-misc/rr-statues-replacer
        assets-misc/better-waterfalls
        assets-misc/waterfalls-tweaks
        assets-misc/new-gondolier-helm
        assets-misc/hirez-armors
        items-weapons/correct-iron-warhammer
        assets-textures/tyddys-armours-retexture
        assets-textures/vurts-silt-strider-retexture
        assets-meshes/cliffracer-replacer
        assets-textures/tyddys-guars
        assets-meshes/mudcrab-replacer
        assets-meshes/4thunknowns-creatures
        creatures-misc/blighted-animals-retextured
        assets-meshes/scamp-replacer
        arch-towns/unique-tavern-signs-tr
        arch-misc/ports-of-vvardenfell
        assets-textures/apels-fire-retexture
        assets-textures/apels-lighthouse-retexture
        arch-towns/rr-lighthouse-tel-branora
        arch-towns/rr-lighthouse-tel-vos
        arch-misc/tower-of-vos
        arch-misc/stav-gnisis-minaret
        skies/swg-skies
        skies/new-starfields
        skies/dying-worlds
        ui/monochrome-user-interface
        media-video/hd-intro
        ui/widescreen-alaisiagae-splash-screens
        ui/mlse
        ui/hd-splash-and-menu
        assets-textures/full-dwemer-retexture
        assets-meshes/fix-those-bastard-rope-fences
        assets-textures/petes-journal
        media-audio/better-sounds
        assets-meshes/mushroom-tree-replacer
        assets-misc/hackle-lo-fixed
        assets-misc/pherim-fire-fern
        assets-misc/cornberry-replacer
        gameplay-weapons/weapon-sheathing
        arch-dungeons/mines-and-caverns
        gameplay-misc/true-lights-and-darkness
        items-misc/glowing-flames
        ui/hd-texture-buttons
        modules/openmw-fonts
        media-fonts/pelagiad
        media-fonts/ayembedt
        media-fonts/dejavu
        land-flora/vurts-groundcover[minimal,-solstheim]
        land-flora/remiros-groundcover[minimal,-solstheim]
        land-flora/aesthesia-groundcover[-bloodmoon]
        virtual/merged-plugin
        assets-textures/vanilla-based-paper-lanterns
        assets-misc/dwemer-puzzle-box-replacer
        land-misc/signposts-retextured
        assets-textures/high-res-skaal
        items-misc/septim-gold-and-dwemer-dumacs
        assets-misc/kets-swirlwood-furniture
        assets-misc/kets-potions-and-beverages
        assets-misc/apels-various-things-sacks
        items-misc/akulakhan-replacer
        assets-misc/caverns-bump-mapped
        assets-textures/r-zero-quill
        assets-textures/aesthesia-stronghold
        assets-textures/road-marker
        npcs-bodies/khajiit-head-pack
        items-clothes/better-clothes
        assets-textures/robe-overhaul
        npcs-misc/divine-vivec
        npcs-misc/better-almalexia
        creatures-misc/golden-saint-variety
    """
    # TODO:
    # https://gitlab.com/portmod/openmw-mods/-/issues/132
