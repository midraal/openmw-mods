# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import Pybuild1, InstallDir, File
from common.nexus import NexusMod
from common.util import CleanPlugin


class Package(CleanPlugin, NexusMod, Pybuild1):
    NAME = "On the move - the Ashlander Tent Deluxe remod"
    DESC = "Adds a portable nomadic dwelling"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/45035"
    # From the README: Permissions: Yes.
    # Unclear what this means.
    LICENSE = "all-rights-reserved"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        tr? ( landmasses/tamriel-rebuilt )
        !tr? ( !!landmasses/tamriel-rebuilt )
    """
    DEPEND = "base/morrowind"
    KEYWORDS = "openmw"
    SRC_URI = """
        On_the_move_v.1.1-45035-1-1.7z
        music? ( Music_files-45035-1-0.7z )
        Hotfix_2-45035-.7z
    """
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/45035"
    IUSE = "tr music"
    TEXTURE_SIZES = "1024"
    INSTALL_DIRS = [
        InstallDir(".", S="Music_files-45035-1-0", REQUIRED_USE="music"),
        InstallDir(
            ".",
            PLUGINS=[
                File("On the Move.esp"),
                # File("On the Move_nom.esp", REQUIRED_USE="nom"),
            ],
            BLACKLIST=["On the Move_nom.esp"],
            S="On_the_move_v.1.1-45035-1-1",
        ),
        InstallDir(
            "ZzAddons",
            PLUGINS=[
                # Requires Morrowind Acoustic Overhaul
                # File("OTM_MAO_addon.ESP"),
                # Requires MWSE
                # File("bjam_Aragon_at_MWSEpatch_v1.0.esp"),
                File("OTM_TR.ESP", REQUIRED_USE="tr"),
            ],
            BLACKLIST=["OTM_MAO_addon.ESP", "bjam_Aragon_at_MWSEpatch_v1.0.esp"],
            S="On_the_move_v.1.1-45035-1-1",
        ),
        # InstallDir(
        #     "ZzAlternative textures/Brown Leather/Data Files",
        #     S="On_the_move_v.1.1-45035-1-1",
        # ),
        # InstallDir(
        #     "ZzAlternative textures/Brown Fur/Data Files",
        #     S="On_the_move_v.1.1-45035-1-1",
        # ),
        # InstallDir(
        #     "ZzAlternative textures/Arctic/Data Files",
        #     S="On_the_move_v.1.1-45035-1-1",
        # ),
        # InstallDir(
        #     "ZzAlternative textures/Arctic Alt/Data Files",
        #     S="On_the_move_v.1.1-45035-1-1",
        # ),
        # InstallDir(
        #     "ZzAlternative textures/Vanilla/Data Files",
        #     S="On_the_move_v.1.1-45035-1-1",
        # ),
        InstallDir("Data Files", S="On_the_move_v.1.1-45035-1-1"),
        InstallDir("Data Files", S="Hotfix_2-45035-"),
    ]

    def src_prepare(self):
        self.clean_plugin("On the Move.esp")
        # clean_plugin("On the Move_nom.esp")
        # clean_plugin("ZzAddons/bjam_Aragon_at_MWSEpatch_v1.0.esp")
